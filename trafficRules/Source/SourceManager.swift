//
//  SourceManager.swift
//  trafficRules
//
//  Created by Nazar Lysak on 23.10.2021.
//

import Foundation

// MARK: - SourceManager

class SourceManager {
    
    func setupViolators() -> [ViolatorModel] {
        let violator1 = ViolatorModel(name: "Nazar", surname: "Lysak", fine: 250.70, infoAboutFine: "За порушення правил користування ременями безпеки або мотошоломами")
        let violator2 = ViolatorModel(name: "Petro", surname: "Naburov", fine: 2300, infoAboutFine: "Штраф за перевищення встановлених обмежень швидкості руху транспортних засобів більш як на п’ятдесят кілометрів на годину")
        let violator3 = ViolatorModel(name: "Ivan", surname: "Petriv", fine: 123, infoAboutFine: "Штраф за керування транспортним засобом особою, яка не має права керування таким транспортним засобом")
        let violator4 = ViolatorModel(name: "Roman", surname: "Lysak", fine: 380.20, infoAboutFine: "Керування транспортним засобом особою, позбавленою права керування транспортним засобом")
        let violator5 = ViolatorModel(name: "Roman", surname: "Pavl", fine: 420, infoAboutFine: "За відсутність водійських прав")
        let violator6 = ViolatorModel(name: "Sergiy", surname: "Kruk", fine: 1500, infoAboutFine: "За створення аварійної ситуації")
        let violator7 = ViolatorModel(name: "Anna", surname: "Pavliv", fine: 400, infoAboutFine: "За водіння у нетверезому стані")
        let violator8 = ViolatorModel(name: "Julia", surname: "Gorner", fine: 590.21, infoAboutFine: "За скоєння ДТП в стані алкогольного сп'яніння з потерпілим")
        let violator9 = ViolatorModel(name: "Yarik", surname: "Bachin", fine: 2000.40, infoAboutFine: "За залишення місця дорожньо-транспортної пригоди")
        let violator10 = ViolatorModel(name: "Greta", surname: "Lysak", fine: 200, infoAboutFine: "За порушення правил користування ременями безпеки або мотошоломами")

        return [violator1, violator2, violator3, violator4, violator5, violator6, violator7, violator8, violator9, violator10]
    }
    
}
