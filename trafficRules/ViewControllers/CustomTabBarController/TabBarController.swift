//
//  TabBarController.swift
//  trafficRules
//
//  Created by Nazar Lysak on 23.10.2021.
//

import UIKit

class TabBarController: UITabBarController {

    @IBOutlet var mainTabBar: UITabBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar.backgroundColor = .white
    }
}
