//
//  ViolatorModel.swift
//  trafficRules
//
//  Created by Nazar Lysak on 23.10.2021.
//

import Foundation

// MARK: - ViolatorModel

struct ViolatorModel {
    var name: String
    var surname: String
    var fine: Double
    var infoAboutFine: String
}
