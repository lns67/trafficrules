//
//  ViolatorsViewController.swift
//  trafficRules
//
//  Created by Nazar Lysak on 23.10.2021.
//

import UIKit

// MARK: - ViolatorsViewController

class ViolatorsViewController: UIViewController {

    @IBOutlet weak var violatorsTableView: UITableView!
    
    var violatorsArray: [ViolatorModel] = []
    let violatorCellID = "ViolatorsTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        violatorsTableView.register(UINib(nibName: violatorCellID, bundle: nil), forCellReuseIdentifier: violatorCellID)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if violatorsArray.isEmpty {
            violatorsArray = SourceManager().setupViolators()
        }
        violatorsTableView.reloadData()
    }
    
}
