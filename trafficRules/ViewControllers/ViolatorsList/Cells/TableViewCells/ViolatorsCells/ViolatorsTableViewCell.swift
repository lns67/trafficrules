//
//  ViolatorsTableViewCell.swift
//  trafficRules
//
//  Created by Nazar Lysak on 23.10.2021.
//

import UIKit

// MARK: - ViolatorsTableViewCell

class ViolatorsTableViewCell: UITableViewCell {

    @IBOutlet weak var sumFullnameLabel: UILabel!
    @IBOutlet weak var infoAboutFineLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func update(violators: ViolatorModel) {
        sumFullnameLabel.text = "\(violators.fine)uah - \(violators.surname) \(violators.name)"
        infoAboutFineLabel.text = violators.infoAboutFine
    }
    
}
