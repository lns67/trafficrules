//
//  ViolatorsViewController+Extensions.swift
//  trafficRules
//
//  Created by Nazar Lysak on 23.10.2021.
//

import Foundation
import UIKit

// MARK: - ViolatorsViewController + Extensions

extension ViolatorsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return violatorsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: violatorCellID, for: indexPath) as! ViolatorsTableViewCell
        cell.update(violators: violatorsArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .delete
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            var deletedViolators: [ViolatorModel] = []
            
            let deletedVC = (self.tabBarController?.viewControllers?[2] as? DeletedViolatorsViewController)
            deletedViolators = deletedVC?.deletedViolatorsArray ?? []
            deletedViolators.append(violatorsArray[indexPath.row])
            
            violatorsArray.remove(at: indexPath.row)
            
            tableView.deleteRows(at: [indexPath], with: .fade)
            deletedVC?.deletedViolatorsArray = deletedViolators
            
        }
    }
}
