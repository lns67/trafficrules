//
//  DeletedViolatorsViewController+Extensions.swift
//  trafficRules
//
//  Created by Nazar Lysak on 23.10.2021.
//

import Foundation
import UIKit

// MARK: - DeletedViolatorsViewController + Extensions

extension DeletedViolatorsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deletedViolatorsArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: violatorCellID, for: indexPath) as! ViolatorsTableViewCell
        cell.update(violators: deletedViolatorsArray[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
