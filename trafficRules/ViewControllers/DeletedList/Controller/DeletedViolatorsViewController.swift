//
//  DeletedViolatorsViewController.swift
//  trafficRules
//
//  Created by Nazar Lysak on 23.10.2021.
//

import UIKit

// MARK: - DeletedViolatorsViewController

class DeletedViolatorsViewController: UIViewController {

    @IBOutlet weak var deletedTableView: UITableView!
    
    var deletedViolatorsArray: [ViolatorModel] = []
    let violatorCellID = "ViolatorsTableViewCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        deletedTableView.register(UINib(nibName: violatorCellID, bundle: nil), forCellReuseIdentifier: violatorCellID)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        deletedTableView.reloadData()
    }
    
}
