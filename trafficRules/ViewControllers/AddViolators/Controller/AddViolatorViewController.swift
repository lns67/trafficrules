//
//  AddViolatorViewController.swift
//  trafficRules
//
//  Created by Nazar Lysak on 23.10.2021.
//

import UIKit

// MARK: - AddViolatorViewController

class AddViolatorViewController: UIViewController {

    @IBOutlet weak var surnameTextField: UITextField!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var fineTextField: UITextField!
    @IBOutlet weak var infoTextField: UITextField!
    
    var violatorsArray: [ViolatorModel] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func addViolatorAction(_ sender: Any) {
        guard let surname = surnameTextField.text, let name = nameTextField.text, let fine = fineTextField.text, let info = infoTextField.text, !surname.isEmpty, !name.isEmpty, !fine.isEmpty, !info.isEmpty else {
            let alertVC = UIAlertController(title: "Error", message: "Please, enter all fields!", preferredStyle: .alert)
            let cancelAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertVC.addAction(cancelAction)
            present(alertVC, animated: true, completion: nil)
            return
        }
        
        let addedViolator = ViolatorModel(name: name, surname: surname, fine: Double(fine) ?? 0, infoAboutFine: info)
        
        let violatorVC = (self.tabBarController?.viewControllers?[0] as? ViolatorsViewController)
        self.violatorsArray = violatorVC?.violatorsArray ?? []
        self.violatorsArray.append(addedViolator)
        violatorVC?.violatorsArray = self.violatorsArray
        self.tabBarController?.selectedIndex = 0
        
        surnameTextField.text = ""
        nameTextField.text = ""
        fineTextField.text = ""
        infoTextField.text = ""
    }
}
